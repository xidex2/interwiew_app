package sk.ness.interview.dto;

import sk.ness.interview.domain.Article;
import sk.ness.interview.domain.Comment;

import java.util.List;

public class ArticleWithComments {
	private Article article;
	private List<Comment> comments;

	public ArticleWithComments(Article article, List<Comment> comments) {
		this.article = article;
		this.comments = comments;
	}

	public Article getArticle() {
		return article;
	}

	public List<Comment> getComments() {
		return comments;
	}
}
