package sk.ness.interview.service;

import sk.ness.interview.domain.Comment;

import java.util.List;

public interface CommentService {
	public List<Comment> findByArticleId(Integer articleID);
}
