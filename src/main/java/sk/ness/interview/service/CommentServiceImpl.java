package sk.ness.interview.service;

import org.springframework.stereotype.Service;
import sk.ness.interview.dao.CommentDAO;
import sk.ness.interview.domain.Comment;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CommentServiceImpl implements CommentService {

	@Resource
	private CommentDAO commentDAO;

	@Override
	public List<Comment> findByArticleId(Integer articleID) {
		return commentDAO.findByArticleId(articleID);
	}
}
