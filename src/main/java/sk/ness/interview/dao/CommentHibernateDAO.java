package sk.ness.interview.dao;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import sk.ness.interview.domain.Comment;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class CommentHibernateDAO implements CommentDAO {

	@Resource(name = "sessionFactory")
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> findByArticleId(Integer articleId) {
		return sessionFactory.getCurrentSession().createSQLQuery("select * from comments where article_id = " + articleId).list();
	}
}
