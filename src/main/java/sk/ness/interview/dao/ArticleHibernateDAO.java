package sk.ness.interview.dao;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import sk.ness.interview.domain.Article;

import javax.annotation.Resource;
import java.util.List;

/**
 * DAO for {@link Article} related DB operations
 *
 * @author michal.kmetka
 *
 */
@Repository
public class ArticleHibernateDAO implements ArticleDAO {

  @Resource(name = "sessionFactory")
  private SessionFactory sessionFactory;

  @Override
  public Article findByID(final Integer articleId) {
    return (Article) this.sessionFactory.getCurrentSession().get(Article.class, articleId);
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Article> findAll() {
    return this.sessionFactory.getCurrentSession().createSQLQuery("select * from articles").addEntity(Article.class).list();
  }

  @Override
  public void persist(final Article article) {
    this.sessionFactory.getCurrentSession().saveOrUpdate(article);
  }

	@SuppressWarnings("unchecked")
	@Override
	public List<Article> findAllWithText(String searchText) {
		Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Article.class);
		criteria.add(Restrictions.or(Restrictions.like("author", searchText, MatchMode.ANYWHERE),
				Restrictions.like("title", searchText, MatchMode.ANYWHERE),
				Restrictions.like("text", searchText, MatchMode.ANYWHERE)));
		return criteria.list();
	}

}
