package sk.ness.interview.dao;

import sk.ness.interview.domain.Comment;

import java.util.List;

public interface CommentDAO {

	public List<Comment> findByArticleId(Integer articleId);
}
